<?php
/**
 * @file
 * Administration page callbacks for the bookable module.
 */

/**
 * Form builder. Configure annotations.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function bookable_admin_settings() {
  // Get an array of node types with internal names as keys and
  // "friendly names" as values. E.g.,
  // array('page' => 'Page', 'story' => 'Story')
  $options = node_get_types('names');

  $form['bookable_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Users may book these content types'),
    '#options' => $options,
    '#default_value' => variable_get('bookable_node_types', array('resource')),
    '#description' => t('Links will be placed to allow users to book the above content types'),
  );


  return system_settings_form($form);

}